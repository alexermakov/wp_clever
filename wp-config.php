<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'clevelbots__x' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~z&T>:I*A3:wz&U!4$5#]9bmu+6$|9$&{B3[,Zm~ 0t{1.dArt_hBsn8_^T?=HEa' );
define( 'SECURE_AUTH_KEY',  '(*4^9H4{<0n`w*Vw%8c[c_FtvXlbr~_`q-[/2ER#1IEx9K-<U56Eo<Vm]Q`TSX6+' );
define( 'LOGGED_IN_KEY',    '+gK1b/}e5&= 5Irs|}^(rK@242%)Y=,.FRgfa$G<Zz<0?KlpMeF=d?urTG.r{`gB' );
define( 'NONCE_KEY',        'zL(osh}96AxTh37=~.*;|^vp+q/0%oKZ`#2]cJ,Hf2)u<I/z0t7)(1LWtq|royTD' );
define( 'AUTH_SALT',        '=xKI39=G;X32AW`l*2kPBU;a c6-O6~3aj~V<`N?j~/<C`nE<PiykW0HqxA-;PZz' );
define( 'SECURE_AUTH_SALT', ']Xz~K&GS<%focp:7?C.TM2jZ,5!)0+DU3@TfG[t]9gvl1LW?3JlwI^RkF$,bY@1m' );
define( 'LOGGED_IN_SALT',   'x7N[2.&3jfCqNjj Z>uCBWW)nOPZj1h^Ar+GUHCq<>G, yXr@K8rw y`9:/d48uZ' );
define( 'NONCE_SALT',       '!A@O2A_cK@%iAC4Bi QgVvHq2|d$X.@XI%P)icvI~g6-qe^O5p1ZL-~R`$%~4x:)' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

define( 'WP_POST_REVISIONS', 3 );
/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
