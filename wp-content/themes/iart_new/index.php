<?php get_header();?>
<div class="home__top__wrap">

    <?php get_template_part('parts/header__block'); ?>

    <section class="section_x section_x__top">
        <div class="container">
            <?php get_template_part('parts/components/side_line'); ?>
            <div class="section_x__top__info">
                <div class="section_x__top__line">
                    <div class="section_x__top__line__title">AI-based solutions for business</div>
                    <div class="section_x__top__line__x"></div>
                    <div class="section_x__top__line__add_text">
                        artificial <br>
                        intelligence
                    </div>
                </div>
                <div class="section_x__top__main_info">
                    <h1 class="section_x__top__title">
                        <?= __('Ваш бизнес-партнер <br>
						по искусственному интелекту');?>
                    </h1>
                    <div class="section_x__top__text"><?= __('Оказываем полный спектр услуг от консалтинга до интеграции и поддержки ИИ-решений');?></div>
                    <div class="section_x__top__btns">
                        <a href="" class="btn_default btn_style_1 section_x__top__btn"><?= __('Получить консультацию');?></a>
                        <a href="" class="btn_default btn_ghost section_x__top__btn"><?= __('Подобрать решение');?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>



<section class="section_x section_x__category js_ancor__category">
    <div class="container">
        <div class="home__category__title">
            <?= __('Получите сегодня преимущества будущего
			<span class="home__category__title--purple">благодаря проверенным и эффективным технологиям</span>');?>
        </div>
        <div class="home__category__list">
            <?php $ids = [3,4,5,2];?>
            <?php foreach ($ids as $id):?>
            <?php $term = get_term( $id, 'product__type' );?>
            <div class="home__category__item">
                <?php $image = get_field('image_preview','product__type_'.$id) ;?>
                <img src="<?= $image['url'];?>" class='home__category__item__image'>
                <div class="home__category__item__info">
                    <div class="home__category__item__info__title"><?= $term->name;?></div>
                    <div class="home__category__item__info__text"><?= get_field('short_text','product__type_'.$id) ;?></div>
                    <a href="<?= get_term_link($term);?>" class="btn_default btn_style_1 btn_home__category"><?= __('Смотреть проекты');?></a>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</section>




<?php get_template_part('parts/companies'); ?>


<section class="section_x section_x__smi" style='display:none'>
    <div class="container">
        <div class="home__company__title">СМИ О НАС</div>
        <div class="section_x__smi__wrap">
            <div class="section_x__smi__list js_section_x__smi__list">
                <?php for ($i=0; $i < 10; $i++):?>
                <div class="section_x__smi__item">
                    <a href="" target='_blank'>
                        <div class="section_x__smi__item__image">
                            <img src="<?= get_template_directory_uri(); ?>/images/home/smi/<?= $i % 5 +1;?>.svg">
                        </div>
                    </a>
                </div>
                <?php endfor;?>
            </div>
            <div class="section_x__smi__arrows js_section_x__smi__arrows">
                <button class="section_x__smi__arrow">
                    <img src="<?= get_template_directory_uri(); ?>/images/icons/arrows/prev.svg">
                </button>
                <div class="section_x__smi__arrows__divide"></div>
                <button class="section_x__smi__arrow">
                    <img src="<?= get_template_directory_uri(); ?>/images/icons/arrows/next.svg">
                </button>
            </div>
        </div>
    </div>
</section>


<?php
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => 10,
        'orderby' => 'rand',
    );
    $the_query = new WP_Query( $args );
?>


<section class="section_x section_x__news js__scroll_wrap">
    <div class="top_news__line top_news__line--another_news">
        <div class="container">
            <div class="top_news__title__"><?= __('Новости Cleverbots');?></div>
            <div class="top_news__line__info">
                <p class='section_x__news__date__uppercase'>
                    <b><?= __('Сегодня');?>:</b> <?= getDateWeekName(0,date('w'));?>
                </p>
                <p><?= date('d');?>, <?= getNameMonth(0,intval(date('m'))-1);?>, <?= date('Y г, H:i:s');?></p>
            </div>
        </div>
    </div>
    <div class="another__news__list">
        <div class="another__news__list__inner decore__scroll__wrap">
            <div class="new__item_y_wrap js__scrollbar js__scrollbar_padding">
                <?php while ( $the_query->have_posts() ): ?>
                <?php $the_query->the_post(); ?>
                <div class="new__item_y">
                    <?php get_template_part('parts/news/slider'); ?>
                </div>
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
    <div class="archive_link__block">
        <div class="container">
            <div class="archive_link__inner">
                <a href="<?= get_term_link(1, 'category');?>" class="archive_link__x">
                    <span><?= __('Архив новостей');?></span>
                    <img src="<?= get_template_directory_uri(); ?>/images/icons/arrow_pagination/next.svg">
                </a>
            </div>
        </div>
    </div>
</section>





<?php get_template_part('parts/callback'); ?>


<?php get_footer();?>