<?php

    // show_admin_bar(false);

    function get_breadcrumb() {
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<div class="breadcrumbs"><div class="container"><div class="breadcrumbs__inner"><ul><li>','</li></ul></div></div></div>');
        }
    }

    add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
    function my_navigation_template( $template, $class ){
        return '
        <nav class="pagination_block %1$s" role="navigation">
		    <div class="container">
			    <div class="pagination_block__inner">
                    <div class="pagination_block__pages">%3$s</div>
                </div>
            </div>
        </nav>
        ';
    }



	function getDateWeekName($lang,$n){
		$nameDayOfWeek = [
			['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
			['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
		];
		return $nameDayOfWeek[$lang][$n];
	}

    function getNameMonth($lang,$n){
        $nameMoth = [
			['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			['January','February','March', 'April', 'May','June', 'July', 'August','September', 'October', 'November','December']
		];
		return $nameMoth[$lang][$n];
    }




    add_theme_support( 'post-thumbnails');

    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }

    add_filter('excerpt_more', function($more) {
        return '...';
    });

    add_filter( 'excerpt_length', function(){
        return 25;
    });



    add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );
    function prefix_remove_default_images( $sizes ) {
        unset( $sizes['medium']);
        unset( $sizes['medium_large']);
        return $sizes;
    }


    function wps_display_attachment_settings() {
        update_option( 'image_default_align', 'none' );
        update_option( 'image_default_link_type', 'none' );
        update_option( 'image_default_size', 'large' );
    }
    add_action( 'after_setup_theme', 'wps_display_attachment_settings' );


    function my_remove_menu(){
        remove_menu_page( 'edit-comments.php' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'users.php' );
        remove_menu_page( 'upload.php' );

    }
    add_action( 'admin_menu', 'my_remove_menu' );




    register_nav_menus(array(
        'main_menu' => 'Главное меню',
        'top'    => 'Верхнее меню',
    ));


    function add_scripts() {
        if(is_admin()) return false;
        wp_deregister_script('jquery');

        wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js','','',true);
        wp_enqueue_script('simplebarjs', 'https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js','','',true);
        wp_enqueue_script('fancybox_js','https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js','','',true);
        wp_enqueue_script('slick_js','https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js','','',true);
        wp_enqueue_script('inputmask', get_template_directory_uri().'/js/jquery.inputmask.bundle.js?1119','','',true);
        wp_enqueue_script('selectric', get_template_directory_uri().'/js/jquery.selectric.min.js','','',true);
        wp_enqueue_script('main', get_template_directory_uri().'/js/main.js?'.uniqid(),'','',true);
        // wp_enqueue_script('bot', get_template_directory_uri().'/js/bot.js?12332242','','',true);
    }

    function add_styles() {
        if(is_admin()) return false;
        wp_enqueue_style( 'slick_css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css' );
        wp_enqueue_style( 'fancybox_css', 'https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css' );
        wp_enqueue_style( 'simplebar', 'https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css' );
        wp_enqueue_style( 'main', get_template_directory_uri().'/css/main.min.css?'.uniqid() );
    }

    add_action('wp_footer', 'add_scripts');
    add_action('wp_print_styles', 'add_styles');


    get_template_part('parts/in_function/mydefault');
    get_template_part('parts/in_function/remove_commets');
    get_template_part('parts/in_function/registrate_data');
    get_template_part('parts/in_function/custom-block_editor');






    function filter_plugin_updates( $update ) {
        $DISABLE_UPDATE = array( 'advanced-custom-fields-pro-master' );
        if( !is_array($DISABLE_UPDATE) || count($DISABLE_UPDATE) == 0 ){  return $update;  }
        foreach( $update->response as $name => $val ){
            foreach( $DISABLE_UPDATE as $plugin ){
                if( stripos($name,$plugin) !== false ){
                    unset( $update->response[ $name ] );
                }
            }
        }
        return $update;
    }
    add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

?>