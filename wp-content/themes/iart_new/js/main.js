function getBodyScrollTop() {
    return self.pageYOffset || (document.documentElement && document.documentElement.ScrollTop) || (document.body && document.body.scrollTop);
}

$(function () {

    $('.js__scrollbar').each(function () {
        new SimpleBar($(this)[0],{ autoHide: false });
    })

    $('.js_faq__btn__drop').click(function (e) {
        e.preventDefault();
        const $parent = $(this).parents('.faq__block__item');
        $parent.toggleClass('active');
        $parent.find('.js_faq__block__item__answer').stop().slideToggle(400);
    })

    $('.js_modal__menu__list__btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).parent('li').find('.modal__menu__list__block').toggleClass('active')
    })

    $('.js_modal__menu__list__block__back').click(function (e) {
        e.preventDefault();
        $(this).parents('li').find('.js_modal__menu__list__btn').toggleClass('active')
        $(this).parents('li').find('.modal__menu__list__block').toggleClass('active')
    })

    $('.js_modal__menu__list__btn_2').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        const el = $(this).parent().parent();
        $(this).parent().find('.modal__menu__list--sub_menu_2').slideToggle(400,function(){
            setHeightMobileMenu(el.height());
        });



    })



    function setHeightMobileMenu(ElHeight){
        $('.js_modal__menu__bottom').removeAttr('style');

        const topHeight = parseInt($('.modal__menu__top').css('margin-top')) + parseInt($('.modal__menu__top').css('margin-bottom')) + $('.modal__menu__top').height();

        const height = ($('.js_modal__menu').outerHeight() - ElHeight -  topHeight)*-1;

        console.log(height)
        console.log( ElHeight - $(window).height())
        console.log('---')


        if (height>0){
            $('.js_modal__menu__bottom').css('margin-bottom',height)
        }
    }







    $('.js_select').selectric({
        nativeOnMobile: false,
    })

    $('.js__single_slider').slick({
        dots: true,
        arrows: false,
        infinite: false
    })

    $('.js_project__x__slider').slick({
        dots: true,
        infinite: false,
        prevArrow: $('.js_project__x__arrows .project__x__arrow:first-child'),
        nextArrow: $('.js_project__x__arrows .project__x__arrow:last-child'),
        responsive: [
            {
              breakpoint: 950,
              settings: {
                arrows: false,
              }
            }
          ]
    })



    function vacancyTextareaMax(elem,e){
        const maxCount = 1000;
        const curentValue = elem.val()
        let currentCount = curentValue.length;
        if(currentCount>=maxCount) {
            currentCount = 1000;
            const maxCutValue = curentValue.slice(0, maxCount);
            elem.val(maxCutValue);
            if (e.keyCode >= 48 && e.keyCode <= 57) return false;
        }
        $('.js_textarea__vacancy__count_symbol').text(currentCount)
    }

    $('.js_textarea__vacancy').keydown(function (e) {
        vacancyTextareaMax($(this),e);
    });

    $(".js_textarea__vacancy").bind("paste", function(e){
        setTimeout(() => {
            vacancyTextareaMax($(this),e)
        }, 100);

    })





    $('.js_mega__menu').hover(function () {
        $('.js_sub_menu__' + $(this).data('megamenu')).addClass('active');
        $(this).addClass('--hover');
    }, function () {
        function removeMegaMenu(menu_link, menuContainer) {
            menu_link.removeClass('--hover');
            menuContainer.removeClass('active');
        }
        let hover__on__menu = false;
        let menu_link = $(this);
        let menuContainer = $('.js_sub_menu__' + $(this).data('megamenu'));
        menuContainer.hover(function () {
            hover__on__menu = true;

        }, function () {
            removeMegaMenu(menu_link, menuContainer)
        });


        setTimeout(() => {
            if (!hover__on__menu) removeMegaMenu(menu_link, menuContainer);
        }, 0);

    });


    $('.js_ancor__two__screen').click(function (e) {
        e.preventDefault();

        $("html, body").animate({
            scrollTop: $('.js_ancor__category').offset().top
        }, "slow");
    })

    $('.js_btn_show__companies').click(function (e) {
        e.preventDefault();
        let timeDelay = 200;
        $('.js_home__companies .home__company_item').each(function () {
            if (!$(this).is(':visible')) {
                $(this).addClass("hide_x")
                $(this).slideDown(400, function () {
                    setTimeout(() => {
                        $(this).removeClass('hide_x');
                    }, timeDelay);
                    timeDelay += 400;
                });

            }
        });
        $(this).fadeOut(400, function () {
            $(this).remove();
        });
    })


    $('.js_btn__up').click(function (e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    })


    function bodyLock(){
        let topY = getBodyScrollTop();
        $('body').toggleClass('body-lock');
        if ($('body').hasClass('body-lock')) {
            $('body').data('topY', topY);
            $('body').css('top', '-' + topY + 'px');
        } else {
            $('body').removeClass('body-lock');
            $('body').removeAttr('style');
            window.scrollTo(0, $('body').data('topY'));
        }
    }

    $('.js_btn_menu,.js_modal__menu__close,.js_modal__menu__overlay').click(function (e) {
        e.preventDefault();

        bodyLock();

        $('.js_modal__menu__overlay').toggleClass('active');
        $('.js_modal__menu').toggleClass('active');
    });

    $('.js_lang_block__view').click(function (e) {
        e.preventDefault();
        $(this).parents('.js_lang_block').toggleClass('active');
        $(this).parents('.js_lang_block').find('.js_lang_block__list').toggleClass('active');

    });

    $('.js_input__field').focus(function (e) {
        e.preventDefault();
        $(this).parents('label').addClass('active')
    });


    $('.js_input__field').blur(function () {
        if ($(this).val() == '') {
            $(this).parents('label').removeClass('active')
        } else {
            $(this).parents('label').addClass('active')
        }
    });

    $('.js_bot__launch').click(function (e) {
        e.preventDefault();
        Fancybox.show([{ src: "#js_bot_x__modal", type: "inline" }]);
    });

    $('.js_bot__close').click(function (e) {
        e.preventDefault();
        Fancybox.close();
    });

    $('.js_btn_mobile__filter, .js_filter_x__top__close, .js_filter_x__overlay').click(function (e) {
        e.preventDefault();
        bodyLock();

        $('.js_filter_x__overlay').toggleClass('active');
        $('.js_filter_x').toggleClass('active')
    })


    function filterInputView(input){
        $block_wrap = input.parents('.js_filter_x__block__list');
        if ($block_wrap.find('input:checked').length>0){
            $block_wrap.addClass('filter_x__block__list--choosed')
        }
        else{
            $block_wrap.removeClass('filter_x__block__list--choosed')
        }
    }

    $('.js_filter_x__block__list input').change(function (e) {
        e.preventDefault();
        filterInputView($(this))
    });


    $('.js__btn_clear_form').click(function (e) {
        e.preventDefault();
        $('.js_filter_x__block__form input:checkbox').each(function () {
            $(this).prop('checked', false);
            filterInputView($(this))
        });
    })






    const animateBotLaunch = () => {
        setTimeout(() => {
            $('.js_bot__launch').removeClass('hide_launch_bot')
        }, 2500);
    }


    function doScrollbarPadding(){
        $('.js__scrollbar_padding').each(function (index, element) {
            const widthContainer = $(this).closest('.js__scroll_wrap').find('.container').outerWidth();
            const containerPadding = parseInt($(this).closest('.js__scroll_wrap').find('.container').css('paddingLeft'));
            const widthAnother__news = $(this).closest('.js__scroll_wrap').width();
            const paddingScroll = (widthAnother__news - widthContainer) / 2 +containerPadding;


            $(this).find('.simplebar-content>*:first-child').css({marginLeft:paddingScroll+'px'});
            $(this).find('.simplebar-content>*:last-child').css({marginRight:paddingScroll+'px'});

        });
    }

    $(window).resize(function () {
        doScrollbarPadding()
    });


    doScrollbarPadding()

    animateBotLaunch()
});