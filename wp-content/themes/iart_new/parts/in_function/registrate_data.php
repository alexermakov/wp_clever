<?php
add_action( 'init', 'create_my_post_types' );
 function create_my_post_types() {
      register_post_type(
         'projects',
          array(
             'labels' => array( 'name' => __('Проекты'),
             'singular_name' => __( 'Проект' ) ),
             'supports'      => array( 'title', 'thumbnail'),
             'public' => true,
             'has_archive' => true,
         )
      );

      register_post_type(
         'products',
          array(
             'labels' => array( 'name' => __('Продукты'),
             'singular_name' => __( 'Продукт' ) ),
             'supports'      => array( 'title', 'thumbnail','editor'),
             'public' => true,
             'has_archive' => true,
         )
      );

     register_post_type(
        'vacancy',
         array(
            'labels' => array( 'name' => __( 'Вакансии' ),
            'singular_name' => __( 'Вакансия' ) ),
            'supports'      => array( 'title'),
            'public' => true,
            'has_archive' => true,
        )
     );

     register_post_type(
      'personal',
       array(
          'labels' => array( 'name' => __( 'Сотрудники' ),
          'singular_name' => __( 'Сотрудник' ) ),
          'supports'      => array( 'title', 'thumbnail'),
          'public' => true,
          'has_archive' => true,
      )
   );
 }


 function create_my_tax() {
    register_taxonomy(
        'product__type',
        'products',
        array(
            'labels' =>  array(
               'name' => __( 'Направления' ),
               'singular_name' => __( 'Направление' )
            ),
            'hierarchical'=> true,
            'query_var' => true,
        )
    );

    register_taxonomy(
      'projects__type',
      'projects',
      array(
          'labels' =>  array(
             'name' => __( 'Тэги проекта' ),
             'singular_name' => __( 'Тэг проекта' )
          ),
          'hierarchical'=> false,
          'query_var' => true,
      )
  );
}
add_action( 'init', 'create_my_tax' );