<div class="section_x__new_item">
	<a href="<?php the_permalink();?>" class="section_x__new_item__image">
		<?php the_post_thumbnail('large');?>
	</a>
	<div class="section_x__new_item__date"><?= get_the_date("j F, Y");?></div>
	<h3 class="section_x__new_item__title">
		<a href="<?php the_permalink();?>"><?php the_title();?></a>
	</h3>
</div>