<section class="section_x section_x__companies">
	<div class="section_x__companies__bg"></div>
	<div class="section_x__companies__bg__x"></div>
	<div class="container">
		<div class="home__company__title"><?= __('Нам доверяют');?></div>
		<div class="home__companies__list">
			<div class="home__companies__inner js_home__companies">

				<?php $images = get_field('company_logo','option');?>
				<?php foreach( $images as $image ): ?>
					<div class="home__company_item">
						<div class="home__company_item__inner">
							<img src="<?= $image['url'];?>">
						</div>
					</div>
				<?php endforeach; ?>

			</div>
			<button class="js_btn_show__companies btn_show__companies">
				<div class="btn_show__companies__text"><?= __('ПОКАЗАТЬ ЕЩЕ');?></div>
				<img src='<?= get_template_directory_uri(); ?>/images/icons/arrow_up.svg'>
			</button>
		</div>
	</div>
</section>