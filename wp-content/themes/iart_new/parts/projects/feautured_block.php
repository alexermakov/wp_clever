<?php
    $args = array(
        'numberposts'	=> -1,
        'post_type'		=> 'projects',
        'meta_query'	=> array(
            array(
                'key'		=> 'is_feautured',
                'value'		=> true,
                'compare'	=> '='
            ),
        )
    );
?>
<?php $the_query = new WP_Query( $args ); ?>
<?php
    global $hasFeautured;
    $hasFeautured= false;
?>
<?php if( $the_query->have_posts() ): ?>
    <?php $hasFeautured = true;?>
    <div class="page_projects__featured">
        <div class="container">
            <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <?php get_template_part('parts/projects/feautured_item'); ?>
            <?php endwhile; ?>
        </div>
    </div>
    <div class="page_projects__line">
        <img src="<?= get_template_directory_uri(); ?>/images/projects/decore/1.png">
    </div>
<?php endif; ?>
<?php wp_reset_query(); ?>