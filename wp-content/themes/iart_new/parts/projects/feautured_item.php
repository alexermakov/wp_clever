<div class="page_projects__featured__item">
    <div class="page_projects__featured__item__image">
        <?php if (get_field('company_logo')):?>
            <?php $image = get_field('company_logo');?>
            <div class="page_projects__featured__item__image__logo">
                <img src="<?= $image['url'];?>">
            </div>
        <?php endif;?>
        <a href='<?php the_permalink();?>' class="page_projects__featured__item__inner">
            <?php the_post_thumbnail('large');?>
        </a>
    </div>
    <div class="page_projects__featured__item__info">
        <div class="page_projects__featured__item__info__year">
            <span><?= get_field('year');?></span>
        </div>
        <div class="page_projects__featured__item__title">
            <a href="<?php the_permalink();?>"><?php the_title();?></a>
        </div>
        <a href="<?php the_permalink();?>" class="btn_default btn_style_1 btn_projects"><?= __('Подробнее');?></a>
    </div>
</div>