<?php $company = get_field('company');?>

<div class="project_item_i">
    <a href="<?php the_permalink();?>" class="project_item_i__image">
        <div class="project_item_i__image__inner">
            <?php the_post_thumbnail('large');?>
        </div>
    </a>
    <div class="project_item_i__info">
        <div class="project_item_i__cat"><?= $company['name'];?></div>
        <div class="project_item_i__title">
            <a href="<?php the_permalink();?>"><?php the_title();?></a>
        </div>
    </div>
</div>