<?php get_template_part('parts/modal_menu'); ?>

<div class="bot_x__modal modal__x" id="js_bot_x__modal">
    <div class="bot_x__modal__inner">
        <?php get_template_part('parts/components/bot'); ?>
    </div>
</div>

<div class="modal__menu__overlay js_filter_x__overlay"></div>





<div class="sub_menu__products js_sub_menu__products">
    <div class="sub_menu__products__inner">
        <div class="sub_menu__products__list">


            <div class="sub_menu__product_col">
                <div class="sub_menu__product__title">Чат-боты</div>
                <div class="sub_menu__product__list">

                <?php
                    $info = [
                        ['Customer care bot','Автоматизация
                        комуникаций'],
                        ['HR bot','Автоматизация HR-процессов
                        от рекрутинга до адаптации
                        и внутренней коммуникации'],
                        ['Promo bot','Автоматизация HR-процессов
                        от рекрутинга до адаптации
                        и внутренней коммуникации'],
                        ['HCP Bot','Автоматизация коммуникации
                        со специалистами'],
                    ]
                ?>
                 <?php $j=0;?>
                    <?php for ($i=0; $i < count($info); $i++):?>
                        <?php $j++;?>
                        <a href="#" class="sub_menu__product__item">
                            <div class="sub_menu__product__item__info">
                                <div class="sub_menu__product__item_title"><?= $info[$i][0];?></div>
                                <div class="sub_menu__product__item_text"><?= $info[$i][1];?></div>
                            </div>
                            <div class="sub_menu__product__item_image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/menu/big/<?= $j;?>.svg">
                            </div>
                        </a>
                    <?php endfor;?>
                </div>
            </div>


            <div class="sub_menu__product_col">
                <div class="sub_menu__product__title">Интеллектуальный анализ данных</div>
                <div class="sub_menu__product__list">

                <?php
                    $info = [
                        ['Sales forecaster','Автоматизация
                        прогнохирования спроса
                        и оптимизация ассортиментной
                        матрицы'],
                        ['Churny','Прогнозирование оттока
                        пользователей и удержание
                        пресонализированным промо']
                    ]
                ?>
                    <?php for ($i=0; $i < count($info); $i++):?>
                        <?php $j++;?>
                        <a href="#" class="sub_menu__product__item">
                            <div class="sub_menu__product__item__info">
                                <div class="sub_menu__product__item_title"><?= $info[$i][0];?></div>
                                <div class="sub_menu__product__item_text"><?= $info[$i][1];?></div>
                            </div>
                            <div class="sub_menu__product__item_image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/menu/big/<?= $j;?>.svg">
                            </div>
                        </a>
                    <?php endfor;?>
                </div>
            </div>


            <div class="sub_menu__product_col">
                <div class="sub_menu__product__title">Речевые технологии</div>
                <div class="sub_menu__product__list">

                <?php
                    $info = [
                        ['Pharm report','Автоматизированное получение
                        актуальной информации
                        о стоимости и наличии продукта'],
                        ['Call review','Автоматизация входящей
                        и исходящей линии кол-центра']
                    ]
                ?>
                    <?php for ($i=0; $i < count($info); $i++):?>
                        <?php $j++;?>
                        <a href="#" class="sub_menu__product__item">
                            <div class="sub_menu__product__item__info">
                                <div class="sub_menu__product__item_title"><?= $info[$i][0];?></div>
                                <div class="sub_menu__product__item_text"><?= $info[$i][1];?></div>
                            </div>
                            <div class="sub_menu__product__item_image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/menu/big/<?= $j;?>.svg">
                            </div>
                        </a>
                    <?php endfor;?>
                </div>
            </div>

            <div class="sub_menu__product_col">
                <div class="sub_menu__product__title">Компьютерное зрение</div>
                <div class="sub_menu__product__list">

                <?php
                    $info = [
                        ['Traffic recorder','Система аналитики траффика
                        торговой точки и поведения
                        посетителей'],
                        ['Doc Check','Автоматизация бизнес-
                        процессов и задач обработки
                        документов']
                    ]
                ?>
                    <?php for ($i=0; $i < count($info); $i++):?>
                        <?php $j++;?>
                        <a href="#" class="sub_menu__product__item">
                            <div class="sub_menu__product__item__info">
                                <div class="sub_menu__product__item_title"><?= $info[$i][0];?></div>
                                <div class="sub_menu__product__item_text"><?= $info[$i][1];?></div>
                            </div>
                            <div class="sub_menu__product__item_image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/menu/big/<?= $j;?>.svg">
                            </div>
                        </a>
                    <?php endfor;?>
                </div>
            </div>


        </div>
        <div class="sub_menu__products__bottom">
            <a href="" class="btn_default btn_style_1 btn_products__more">Получить консультацию</a>
        </div>
    </div>
</div>