
<div class="modal__menu js_modal__menu">
    <div class="modal__menu__hidden">
        <div class="modal__menu__wrap">
            <div class="modal__menu__main">

                <div class="modal__menu__top">
                    <div class="modal__menu__close js_modal__menu__close"></div>
                    <div class="modal__menu__title"><?= __('Меню');?></div>
                    <a href="#" class="modal__menu__logo">
                        <img src="<?= get_template_directory_uri(); ?>/images/main/logo.svg">
                    </a>
                    <?php get_template_part('parts/components/lang_drop'); ?>
                </div>

                <div class="modal__menu__contact">
                    <a href="tel:<?= get_field('phone','option');?>" class="modal__menu__contact__phone">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19"><g><g>
                            <path fill="#a674ff" d="M17.543 8.7c.04.242.263.424.505.404h.081c.263-.04.445-.303.404-.586a9.572 9.572 0 0 0-2.707-5.229A9.565 9.565 0 0 0 10.613.604c-.283-.06-.525.121-.586.404a.48.48 0 0 0 .404.586 8.632 8.632 0 0 1 4.688 2.422c1.273 1.292 2.121 2.907 2.424 4.684zM10.29 4.844c.99.182 1.9.646 2.627 1.373A4.745 4.745 0 0 1 14.29 8.84c.04.243.263.424.506.424h.08c.263-.04.445-.302.404-.585a6.111 6.111 0 0 0-1.656-3.17 5.705 5.705 0 0 0-3.172-1.655.48.48 0 0 0-.586.404.48.48 0 0 0 .424.585zm5.395 11.387c-.243.242-.506.484-.728.767-.384.424-.848.605-1.434.605h-.182c-1.132-.1-2.182-.524-2.99-.908a16.946 16.946 0 0 1-5.617-4.401C3.44 10.739 2.572 9.286 1.986 7.73c-.364-.949-.485-1.696-.424-2.382.04-.444.222-.808.525-1.13L3.38 2.925c.182-.182.384-.283.586-.283.243 0 .445.142.586.283.242.222.444.444.687.686l.364.364 1.03 1.03c.202.201.303.403.303.605 0 .202-.1.404-.303.606l-.323.323c-.324.323-.627.626-.95.929l-.02.02c-.323.323-.263.646-.202.848 0 .02 0 .02.02.04.263.646.647 1.272 1.233 1.999 1.05 1.292 2.161 2.301 3.374 3.069.161.1.323.181.464.262.142.06.263.141.384.202.02 0 .02.02.04.02.122.06.243.101.364.101.303 0 .505-.202.566-.262l1.313-1.313c.122-.12.344-.282.586-.282.243 0 .425.161.566.282l2.101 2.1c.485.484.263.929-.02 1.211-.121.162-.283.303-.444.465zm1.192.222c.767-.788.767-1.838 0-2.605l-2.102-2.1c-.363-.383-.808-.585-1.293-.585-.464 0-.909.202-1.293.586l-1.212 1.211c-.101-.06-.202-.101-.303-.162-.142-.06-.263-.14-.384-.201-1.131-.707-2.162-1.656-3.132-2.847-.485-.606-.808-1.11-1.03-1.635.303-.283.606-.586.889-.868l.323-.323c.384-.384.606-.848.606-1.313 0-.464-.202-.908-.606-1.312l-1.03-1.05a5.675 5.675 0 0 1-.344-.363 11.663 11.663 0 0 0-.707-.707c-.364-.363-.808-.565-1.273-.565-.464 0-.909.202-1.293.565L1.38 3.491a2.763 2.763 0 0 0-.828 1.757c-.081 1.11.242 2.14.484 2.826.607 1.656 1.536 3.21 2.91 4.866a17.99 17.99 0 0 0 5.96 4.663c.869.424 2.04.909 3.354.99h.242c.87 0 1.617-.323 2.183-.95l.02-.02c.202-.242.424-.464.666-.686.182-.141.344-.303.506-.484z"/></g></g>
                        </svg><?= get_field('phone','option');?>
                    </a>
                    <a href="mailto:<?= get_field('email','option');?>" class="modal__menu__contact__mail">
                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="11" viewBox="0 0 15 11">
                            <g><g><path fill="#fff" d="M1.607 9.9a.543.543 0 0 1-.536-.55V1.771l5.486 3.977a1.62 1.62 0 0 0 1.886 0l5.486-3.977V9.35c0 .304-.24.55-.536.55zm11.395-8.8L7.827 4.851a.587.587 0 0 1-.654 0L1.998 1.1zm.39-1.1H1.608C.72 0 0 .739 0 1.65v7.7C0 10.261.72 11 1.607 11h11.786C14.28 11 15 10.261 15 9.35v-7.7C15 .739 14.28 0 13.393 0z"/></g></g>
                        </svg><?= get_field('email','option');?>
                    </a>
                </div>

                <div class="modal__menu__list">
                    <ul>
                        <li>
                            <a href="<?= get_page_link(22);?>"><?= get_the_title(22);?></a>
                            <div class="modal__menu__list__btn js_modal__menu__list__btn"></div>

                            <div class="modal__menu__list__block">
                                <div class="modal__menu__list__block__top">
                                    <button class="modal__menu__list__block__back js_modal__menu__list__block__back"></button>
                                    <div class="modal__menu__list__block__title">
                                        <span><?= get_the_title(22);?></span>
                                    </div>
                                </div>
                                <ul class="modal__menu__list--sub_menu modal__menu__list--small">
                                    <?php $info = [2,3,4,5];?>
                                    <?php foreach ($info as $id):?>
                                        <?php $term = get_term( $id, 'product__type' );?>
                                        <a href="<?= get_term_link($term);?>">
                                            <?= $term->name;?>
                                        </a>
                                    <?php endforeach;?>
                                </ul>

                                <a href="<?= get_page_link(22);?>" class="btn__default btn_style_1 btn_modal__menu__y">
                                    <?= __('Подробнее');?>
                                </a>

                                <div class="btn_modal__menu__y__bottom"></div>
                            </div>

                        </li>

                        <?php $postype = get_post_type_object('projects');?>
                        <li><a href="<?= get_post_type_archive_link( 'projects' );?>"><?= $postype->labels->name;?></a></li>

                        <li>
                            <?php $postype = get_post_type_object('products');?>
                            <a href="<?= get_post_type_archive_link('products');?>"><?= $postype->labels->name;?></a>
                            <div class="modal__menu__list__btn js_modal__menu__list__btn"></div>

                            <div class="modal__menu__list__block">
                                <div class="modal__menu__list__block__top">
                                    <button class="modal__menu__list__block__back js_modal__menu__list__block__back"></button>
                                    <div class="modal__menu__list__block__title">
                                        <?= $postype->labels->name;?>
                                    </div>
                                </div>
                                <ul class="modal__menu__list--sub_menu">
                                    <li>
                                        <a href="">Чат-боты</a>
                                        <div class="modal__menu__list__btn_2 js_modal__menu__list__btn_2"></div>
                                        <ul class="modal__menu__list--sub_menu_2 modal__menu__list--small">
                                            <?php $info = ['Customer care bot', 'HR bot','Promo bot','HCP Bot'];?>
                                            <?php for ($i=0; $i < count($info); $i++):?>
                                                <li><a href=""><?= $info[$i];?></a></li>
                                            <?php endfor;?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="">Анализ данных</a>
                                        <div class="modal__menu__list__btn_2 js_modal__menu__list__btn_2"></div>
                                        <ul class="modal__menu__list--sub_menu_2 modal__menu__list--small">
                                            <?php $info = ['Sales forecaster','Churny'];?>
                                            <?php for ($i=0; $i < count($info); $i++):?>
                                                <li><a href=""><?= $info[$i];?></a></li>
                                            <?php endfor;?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="">Речевые технологии</a>
                                        <div class="modal__menu__list__btn_2 js_modal__menu__list__btn_2"></div>
                                        <ul class="modal__menu__list--sub_menu_2 modal__menu__list--small">
                                            <?php $info = ['Pharm report','Call review'];?>
                                            <?php for ($i=0; $i < count($info); $i++):?>
                                                <li><a href=""><?= $info[$i];?></a></li>
                                            <?php endfor;?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="">Компьютерное зрение</a>
                                        <div class="modal__menu__list__btn_2 js_modal__menu__list__btn_2"></div>
                                        <ul class="modal__menu__list--sub_menu_2 modal__menu__list--small">
                                            <?php $info = ['Traffic recorder','Doc Check'];?>
                                            <?php for ($i=0; $i < count($info); $i++):?>
                                                <li><a href=""><?= $info[$i];?></a></li>
                                            <?php endfor;?>
                                        </ul>
                                    </li>
                                </ul>
                                <a href="" class="btn__default btn_style_1 btn_modal__menu__y">
                                    <?= __('Подробнее');?>
                                </a>
                                <div class="btn_modal__menu__y__bottom"></div>
                            </div>

                        </li>
                        <li>
                            <a href="<?= get_page_link(6);?>"><?= __('О нас');?></a>
                            <div class="modal__menu__list__btn js_modal__menu__list__btn"></div>

                            <div class="modal__menu__list__block">
                                <div class="modal__menu__list__block__top">
                                    <button class="modal__menu__list__block__back js_modal__menu__list__block__back"></button>
                                    <div class="modal__menu__list__block__title">
                                        <?= __('О нас');?>
                                    </div>
                                </div>
                                <ul class="modal__menu__list--sub_menu modal__menu__list--small">
                                    <li><a href="<?= get_page_link(6);?>"><?= get_the_title(6);?></a></li>
                                    <?php $postype = get_post_type_object('vacancy');?>
                                    <li><a href="<?= get_post_type_archive_link( 'vacancy' );?>"><?= $postype->labels->name;?></a></li>
                                    <li><a href="<?= get_term_link(1, 'category');?>"><?= __('Блог');?></a></li>
                                    <li><a href="<?= get_page_link(19);?>"><?= get_the_title(19);?></a></li>
                                </ul>
                                <a href="" class="btn__default btn_style_1 btn_modal__menu__y">
                                    <?= __('Подробнее');?>
                                </a>
                                <div class="btn_modal__menu__y__bottom"></div>
                            </div>
                        </li>
                        <li><a href="https://marketplace.cleverbots.ru/">Marketplace</a></li>
                    </ul>
                </div>
                <a href="" class="btn_modal__menu__call btn_default"><?= __('Получить консультацию');?></a>
            </div>
            <div class="modal__menu__bottom js_modal__menu__bottom">
                <div class="modal__menu__copyright">
                    © <?= date("Y");?> <?= __('Cleverbots. Все права защищены.');?>
                </div>
                <div class="modal__menu__social">
                    <?php get_template_part('parts/components/social'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal__menu__overlay js_modal__menu__overlay"></div>