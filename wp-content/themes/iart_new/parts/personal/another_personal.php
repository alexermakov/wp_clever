<?php
    $args = array(
        'post_type' => 'personal',
        'posts_per_page' => -1,
        'post__not_in' => array(get_the_ID())
    );
    $the_query = new WP_Query( $args );
?>

<?php if ( $the_query->have_posts() ):?>

    <div class="team__another__man">
        <div class="container">
            <div class="team__another__man__title">
                <?= __('ДРУГИЕ ЧЛЕНЫ КОМАНДЫ');?>
            </div>
            <div class="decore__scroll__wrap">
                <div class="team__another__man__x js__scrollbar js__scrollbar_padding">
                <?php while ( $the_query->have_posts() ): ?>
                    <?php $the_query->the_post(); ?>
                    <?php get_template_part('parts/components/card_team'); ?>
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>

<?php endif;?>