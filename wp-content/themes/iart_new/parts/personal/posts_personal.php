<?php if( have_rows('personal_posts') ): ?>
    <div class="team_enter">
        <div class="container">
            <div class="team_enter__title"><?= __('ВЫСТУПЛЕНИЯ');?></div>
            <div class="decore__scroll__wrap">
                <div class="team_enter__list js__scrollbar">

                    <?php while( have_rows('personal_posts') ): the_row();?>
                        <?php $image = get_sub_field('image'); ?>

                        <div class="team_enter__item">
                            <a href="<?= get_sub_field('link');?>" class="team_enter__item__image">
                                <div class="team_enter__item__inner">
                                    <img src="<?= $image['url'];?>">
                                </div>
                            </a>
                            <div class="team_enter__item__info">

                                <div class="team_enter__item__date">
                                    <?= get_sub_field('year');?>
                                </div>

                                <a href="<?= get_sub_field('link');?>" class="team_enter__item__title">
                                    <?= get_sub_field('name');?>
                                </a>

                            </div>
                        </div>
                    <?php endwhile; ?>

                </div>
            </div>
        </div>
    </div>

<?php endif; ?>