<?php if( have_rows('speech') ): ?>
    <div class="team_publication__block">
        <div class="container">
            <div class="team_publication__block__inner">
                <div class="team_publication__block__title"><?= __('Публикации');?></div>
                <div class="team_publication__block__list">

                    <?php while( have_rows('speech') ): the_row();?>
                        <?php $icon = get_sub_field('icon'); ?>
                        <div class="team_publication__item">

                            <div class="team_publication__item__title">
                                <img src="<?= $icon['url'];?>">
                                <span>"<?= get_sub_field('name');?></span>
                            </div>

                            <div class="team_publication__item__text">
                                <?= get_sub_field('theme');?>
                            </div>

                            <a target='_blank' href="<?= get_sub_field('link');?>" class="team_publication__item__more">
                                <?= __('Подробнее');?>
                            </a>

                            <div class="team_publication__item__year">
                                <?= get_sub_field('year');?>
                            </div>

                        </div>
                    <?php endwhile; ?>

                </div>
            </div>
        </div>
    </div>
<?php endif; ?>