<div class="list__social">
    <?php $social = get_field('socai_list','option');?>
    <a href="<?= $social['tg'];?>" target='_blank'>
		<img src="<?= get_template_directory_uri(); ?>/images/icons/social/color/tg.svg">
		<span>TELEGRAM</span>
	</a>
	<a href="<?= $social['lin'];?>" target='_blank'>
		<img src="<?= get_template_directory_uri(); ?>/images/icons/social/color/id.svg">
		<span>linkedin</span>
	</a>
	<a href="<?= $social['fb'];?>" target='_blank'>
		<img src="<?= get_template_directory_uri(); ?>/images/icons/social/color/fb.svg">
		<span>FACEBOOK</span>
	</a>
</div>