<div class="faq__block">
    <div class="container">
        <div class="faq__block__inner">
            <div class="faq__block__title">Вопросы и ответы</div>
            <div class="faq__block__list">
                <?php $info = ['Название очередного вопроса на сайте?','Еще одно название вопроса на сайте для понимания азвание вопроса на сайте для понимания'];?>
                <?php for ($i=0; $i < 4; $i++) :?>
                    <div class="faq__block__item">
                        <div class="faq__block__item__question">
                            <div class="faq__block__item__question__value">
                                <?= $info[rand(0,1)];?>
                            </div>
                            <button class="faq__block__item__question__btn js_faq__btn__drop"></button>
                        </div>
                        <div class="faq__block__item__answer js_faq__block__item__answer">
                            <div class="faq__block__item__answer__inner">
                                <div class="default__text">
                                    <p>Сайт рыба текст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально - слухового восприятия.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor;?>


            </div>
        </div>
    </div>
</div>