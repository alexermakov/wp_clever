<div class="bot_x__wrap js_bot_x">
    <div class="bot__top">
        <div class="bot__title">Помощник Cleverbots</div>
        <div class="bot__close js_bot__close"></div>
    </div>
    <div class="bot_x js_bot_x">
        <div class="bot__wrap">
            <div class="bot__inner">
                <!--  -->
                <div class="bot__item bot__item--bot">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Здравствуйте!</p>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot bot__item--not_icon">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Ищете подходящее ИИ-решение для Вашего бизнеса?</p>
                        </div>
                    </div>
                </div>


                <!--  -->
                <div class="bot__item bot__item--user">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__buttons bot__item__info__buttons--choosed">
                            <a href="" class='active'>Да</a>
                            <a href="">Нет, просто любопытно</a>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--user">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__buttons">
                            <a href="">Да</a>
                            <a href="">Нет, просто любопытно</a>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Как к вам можно <a href="">обращаться?</a></p>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot bot__item--not_icon">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__form js_bot__item__info__form">
                            <div class="bot__item__info__row">
                                <div class="bot__item__info__field">
                                    <input type="text" name="name" class='js_bot_x__field--name' placeholder="Ваше имя">
                                </div>
                                <button class="bot__item__info__btn">Отправить</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--user">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__answer">
                            Ермаков Алексей
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Там мы рассказываем о том, как ИИ-технологии применяются для решения бизнес-задач</p>
                        </div>
                    </div>
                </div>



                <!--  -->
                <div class="bot__item bot__item--user">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__answer">
                            Круто, спасибо
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Обсудим этот вопрос подробнее?</p>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Как с вами связаться?</p>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--user">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__buttons">
                            <a href="">По телефону</a>
                            <a href="">По email</a>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__text">
                            <p>Оставьте ваши контактные данные, и мы свяжемся с вами в удобное для вас время</p>
                        </div>
                    </div>
                </div>

                <!--  -->
                <div class="bot__item bot__item--bot bot__item--not_icon">
                    <div class="bot__item__image"></div>
                    <div class="bot__item__info">
                        <div class="bot__item__info__form js_bot__item__info__form bot__item__info__form--big">
                            <div class="bot__item__info__quatro">
                                <div class="bot__item__info__field">
                                    <input type="text" name="name" class='js_bot_x__field--name' placeholder="Ваше имя">
                                </div>
                                <div class="bot__item__info__field">
                                    <input type="text" name="name" class='js_bot_x__field--name' placeholder="Название компании">
                                </div>
                                <div class="bot__item__info__field">
                                    <input type="text" name="name" class='js_bot_x__field--name' placeholder="Телефон">
                                </div>
                                <div class="bot__item__info__field">
                                    <input type="text" name="name" class='js_bot_x__field--name' placeholder="Email">
                                </div>
                            </div>
                            <button class="bot__item__info__btn">Отправить</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>