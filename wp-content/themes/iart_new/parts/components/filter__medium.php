<div class="filter__projects--medium">
    <div class="filter__projects--medium__wrap">
        <div class="filter__projects--medium__col_1">
            <div class="filter__projects__select select__filter">
				<select class="js_select">
                <?php
                    $info =['Чат-боты','Анализ данных','Речевые технологии','Компьютерное зрение'];
                ?>
                    <?php for ($i=0; $i < count($info); $i++):?>
                        <option value="<?= $info[$i];?>"><?= $info[$i];?></option>
                    <?php endfor;?>
				</select>
			</div>

            <div class="filter__projects__select select__filter">
				<select class="js_select">
                <?php
                    $info =['HR','Аналитика','Производство','Логистика','Бухглатерия и финансы','Продажи','Маркетинг','Digital & E-commerce','IT'];
                ?>
                    <?php for ($i=0; $i < count($info); $i++):?>
                        <option value="<?= $info[$i];?>"><?= $info[$i];?></option>
                    <?php endfor;?>
				</select>
			</div>
        </div>
        <a href="" class="filter__projects__all__link">Все кейсы</a>
    </div>
    <div class="filter__projects--mobile">
        <button class="btn_mobile__filter js_btn_mobile__filter">
            <img src="<?= get_template_directory_uri(); ?>/images/icons/filter_icon.svg" alt="">
            показать фильтр
        </button>
    </div>
</div>