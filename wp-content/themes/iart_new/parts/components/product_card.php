<div class="product_card">
    <a href="" class="product_card__image__link">
        <div class="product_card__image__link__inner">
            <img src="<?= get_template_directory_uri(); ?>/images/content/product/<?= $info[$i][0];?>">
        </div>
    </a>
    <div class="product_card__info">
        <div class="product_card__info__main">
            <div class="product_card__cat">Чат-боты</div>
            <a href="#" class="product_card__title"><?= $info[$i][1];?></a>
            <div class="product_card__text"><?= $info[$i][2];?></div>
        </div>
        <div class="product_card__triggers">
            <?php for ($j=0; $j < count($info[$j][3]); $j++):?>
                <div class="product_card__trigger">
                    <div class="product_card__trigger__x">
                        <?php if ($info[$i][3][$j][2]!=null):?>
                            <div class="product_card__trigger__x__value--small"><?= $info[$i][3][$j][2];?></div>
                        <?php endif;?>
                        <div class="product_card__trigger__x__value"><?= $info[$i][3][$j][0];?></div>
                    </div>
                    <div class="product_card__trigger__y"><?= $info[$i][3][$j][1];?></div>
                </div>
            <?php endfor;?>
        </div>
        <div class="product_card__info__btns">
            <a href="" class="btn_default btn_style_1  btn_project">Подробнее</a>
            <a href="" class="btn_default btn_ghost btn_project__2">Обсудить проект</a>
        </div>
    </div>
</div>