<div class="back_link__block">
	<div class="container">
		<div class="back_link__inner">
			<a href="javascript:history.back(1);" class="back_link__x">
				<img src="<?= get_template_directory_uri(); ?>/images/icons/arrow_pagination/prev.svg">
				<span>ВЕРНУТЬСЯ НАЗАД</span>
			</a>
		</div>
	</div>
</div>