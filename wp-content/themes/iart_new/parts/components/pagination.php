<?php $args = array(
	    'show_all'     => False,
        'end_size'     => 1,
        'mid_size'     => 1,
        'prev_next'    => true,
        'prev_text'    => __('<img src="'.get_template_directory_uri().'/images/icons/arrow_pagination/prev.svg"><span>Назад</span>'),
        'next_text'    => __('<span>Далее</span><img src="'.get_template_directory_uri().'/images/icons/arrow_pagination/next.svg">'),
        'add_args'     => False,
        'add_fragment' => '',
        'screen_reader_text' => __( ' ' ),
        'num_pages' => 10,
		'step_link' => 10
);	?>

<?= the_posts_pagination( $args ); ?>