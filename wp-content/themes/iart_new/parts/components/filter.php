<div class="filter_x js_filter_x">

    <div class="filter_x__top">
        <div class="filter_x__top__title">Фильтр</div>
        <div class="filter_x__top__col">
            <button class="js_filter_x__top__close filter_x__top__close"></button>
            <button class="filter_x__top__clear__btn js__btn_clear_form">Очистить</button>
        </div>
    </div>

    <div class="filter_x__block__wrap js_filter_x__block__form">


        <div class="filter_x__block">
            <div class="filter_x__block__title">технология</div>
            <div class="filter_x__block__list js_filter_x__block__list">
                <?php
                    $info =['Чат-боты','Анализ данных','Речевые технологии','Компьютерное зрение'];
                ?>
                <?php for ($i=0; $i < count($info); $i++):?>
                    <label class="filter_x__block__item">
                        <input type="checkbox" name="tech" value="<?= $info[$i];?>" >
                        <div class="filter_x__block__item__view"><?= $info[$i];?></div>
                    </label>
                <?php endfor;?>
            </div>
        </div>


        <div class="filter_x__block">
            <div class="filter_x__block__title">НАПРАВЛЕНИЯ</div>
            <div class="filter_x__block__list js_filter_x__block__list">
                <?php
                    $info =['HR','Аналитика','Производство','Логистика','Бухглатерия и финансы','Продажи','Маркетинг','Digital & E-commerce','IT'];
                ?>
                <?php for ($i=0; $i < count($info); $i++):?>
                    <label class="filter_x__block__item">
                        <input type="checkbox" name="tech" value="<?= $info[$i];?>" >
                        <div class="filter_x__block__item__view"><?= $info[$i];?></div>
                    </label>
                <?php endfor;?>

            </div>
        </div>
    </div>

    <a href="" class="filter_all_link">Все кейсы</a>
</div>