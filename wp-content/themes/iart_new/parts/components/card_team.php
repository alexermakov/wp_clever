<div class="team__item">
    <div class="team__item__decore">
        <div class="team__item__decore--top"></div>
        <div class="team__item__decore--bottom"></div>

        <div class="team__item__decore--top team__item__decore--last"></div>
        <div class="team__item__decore--bottom team__item__decore--last"></div>

    </div>
    <div class="team__item__inner">
        <div class="team__item__inner__wrap">
            <div class="team__item__image">
                <?php the_post_thumbnail('full');?>
            </div>
            <div class="team__item__info">
                <div class="team__item__title"><?php the_title();?></div>
                <div class="team__item__ref"><?= get_field('position');?></div>
                <a href="<?php the_permalink();?>" class="btn_default btn_team__item"><?= __('Подробнее');?></a>
            </div>
        </div>
    </div>
</div>