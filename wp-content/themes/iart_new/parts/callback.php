<section class="section_x section_x__callback">
	<div class="container">
		<div class="callback__block">
			<div class="callback__block__info">
				<div class="callback__block__info__inner">
					<div class="callback__block__info__title"><?= __('Обсудить проект');?></div>
					<div class="callback__block__info__text"><?= __('Хотите узнать, как решить вашу задачу с помощью искусственного интеллекта? Заполните форму, и наши менеджеры свяжутся с вами в ближайшее время.');?></div>
					<div class="callback__block__info__label">cleverbots</div>
				</div>
			</div>
			<div class="callback__block__form">
				<form action="javascript:void(0)" class='js_form'>
					<div class="callback__block__field">
						<label>
							<div class="callback__block__field__title"><?= __('Ваше имя');?></div>
							<input type='text' placeholder="<?= __('Укажите ваше имя');?>" class="callback__block__field__x js_input__field" required>
						</label>
					</div>
					<div class="callback__block__field">
						<label>
							<div class="callback__block__field__title"><?= __('Ваш телефон');?></div>
							<input type='tel' placeholder="<?= __('Укажите ваш телефон');?>"class="callback__block__field__x js_input__field" required>
						</label>
					</div>
					<div class="callback__block__field">
						<label>
							<div class="callback__block__field__title"><?= __('Ваш e-mail');?></div>
							<input type='teemailxt' placeholder="<?= __('Укажите вашу почту');?>" class="callback__block__field__x js_input__field" required>
						</label>
					</div>
					<div class="callback__block__bottom">
						<div class="btn_calback_wrap">
							<button class="btn_default btn_calback">
								<span><?= __('Отправить');?></span>
								<img src="<?= get_template_directory_uri(); ?>/images/icons/arrow_send.svg">
							</button>
						</div>
						<div class="callback__block__bottom__disclamer"><?= __('Нажимая отправить,<br> вы даете согласие на обработку');?><br> <a href="<?= get_page_link(3);?>" target="_blank"><?= __('персональных данных');?></a></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>