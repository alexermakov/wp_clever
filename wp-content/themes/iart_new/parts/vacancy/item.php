<div class="vacancy__item">
	<div class="vacancy__item__top">
		<h3 class="vacancy__item__title">
			<a href="<?php the_permalink();?>"><?php the_title();?></a>
		</h3>
		<div class="vacancy__item__date"><?= get_the_date("j F, Y");?></div>
	</div>
	<div class="vacancy__item__text"><?= get_field('short_text');?></div>
	<div class="vacancy__item__bottom">
		<div class="vacancy__item__add">

			<?php if (get_field('type')):?>
				<div class="vacancy__item__add__item">
					<img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/1.svg">
					<span><?= get_field('type');?></span>
				</div>
			<?php endif;?>

			<?php if (get_field('experience')):?>
				<div class="vacancy__item__add__item">
					<img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/2.svg">
					<span><?= get_field('experience');?></span>
				</div>
			<?php endif;?>

			<?php if (get_field('city')):?>
				<div class="vacancy__item__add__item">
					<img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/3.svg">
					<span><?= get_field('city');?></span>
				</div>
			<?php endif;?>

			<?php if (get_field('type_2')):?>
				<div class="vacancy__item__add__item">
					<img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/4.svg">
					<span><?= get_field('type_2');?></span>
				</div>
			<?php endif;?>

		</div>
		<a href="<?php the_permalink();?>" class="vacancy__item__link"><?= __('Подробнее');?></a>
	</div>
</div>