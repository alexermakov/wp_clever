<div class="vacancy__page__x_inner vacancy__page__x_inner--form">
	<div class="vacancy__page__form__title"><?= __('Откликнуться на вакансию');?></div>
	<form action="javascript:void(0)" class="vacancy__page__form">
		<div class="vacancy__page__form__row">
			<div class="callback__block__field">
				<label>
					<div class="callback__block__field__title"><?= __('Ваше имя');?></div>
					<input type='text' placeholder="<?= __('Укажите ваше имя');?>" class="callback__block__field__x js_input__field" required>
				</label>
			</div>
			<div class="callback__block__field">
				<label>
					<div class="callback__block__field__title"><?= __('Ваш телефон');?></div>
					<input type='tel' placeholder="<?= __('Укажите ваш телефон');?>" class="callback__block__field__x js_input__field" required>
				</label>
			</div>
			<div class="callback__block__field">
				<label>
					<div class="callback__block__field__title"><?= __('Ваш e-mail');?></div>
					<input type='mail' placeholder="<?= __('Укажите вашу почту');?>" class="callback__block__field__x js_input__field" required>
				</label>
			</div>
			<div class="callback__block__field">
				<label>
					<div class="callback__block__field__title"><?= __('Ваканcия');?></div>
					<input type='text' class="callback__block__field__x js_input__field" readonly required value='<?php the_title();?>'>
				</label>
			</div>
			<div class="callback__block__field">
				<label>
					<div class="callback__block__field__title"><?= __('Ваш город');?></div>
					<select required placeholder='Выбрать город' class='js_select'>
						<option value="">Город 1</option>
						<option value="">Город 2</option>
						<option value="">Город 3</option>
						<option value="">Город 4</option>
						<option value="">Город 5</option>
						<option value="">Город 6</option>
						<option value="">Город 7</option>
						<option value="">Город 8</option>
						<option value="">Город 9</option>
						<option value="">Город 10</option>
					</select>
				</label>
			</div>
			<div class="callback__block__field callback__block__field--file">
				<label>
					<div class="callback__block__field__title"><?= __('Резюме');?></div>
					<input type="file" name="" id="">
					<div class="callback__block__field__file__view">
						<div class="field__file__view__text"><?= __('Резюме Прикрепить (pdf, doc)');?></div>
						<div class="field__file__view__icon">
							<img src="<?= get_template_directory_uri(); ?>/images/icons/attach.svg">
						</div>
					</div>
				</label>
			</div>
		</div>
		<div class="callback__block__field callback__block__field--textarea">
			<div class="textarea__vacancy__wrap">
				<div class="textarea__vacancy__count_symbol">
					<span class='js_textarea__vacancy__count_symbol'>0</span>/
					<span>1000</span>
				</div>
				<textarea name="" placeholder='Сопроводительное письмо' class='textarea__vacancy js_textarea__vacancy'></textarea>
			</div>
		</div>
		<div class="vacancy__page__form__bottom">
			<button class='btn_default btn_style_1 btn_vacancy__page'><?= __('Отправить');?></button>
			<div class="vacancy__page__form__bottom_text"><?= __('Нажимая отправить, вы даете согласие на обработку');?> <a href=""><?= __('персональных данных');?></a></div>
		</div>
	</form>
</div>