<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>
<?php the_post();?>

<main class="main_x">
    <div class="main_x__inner main_x--decore">
        <?php get_template_part('parts/components/side_line'); ?>
        <div class="top_page">
            <div class="container">
                <h1 class="title__">
                    <?php the_title();?>
                </h1>
                <div class="new_page__info">
                    <div class="new_page__info__date"><?= get_the_date("j F, Y");?></div>
                    <?php $term_list = wp_get_post_terms( $post->ID, 'category' ); ?>
                    <?php if( $term_list ) :?>
                    <div class="new_page__info__hash">
                        <?php foreach( $term_list as $term ):?>
                        <a href="<?= get_term_link( $term );?>">#<?= $term->name;?></a>
                        <?php endforeach;?>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <div class="single__page__wrap">
            <div class="container">
                <div class="single__page__inner">

                    <?php if (get_field('big_photo')):?>
                    <div class="single__page__image">
                        <?php $image = get_field('big_photo'); ?>
                        <img src="<?= $image['url'];?>">
                    </div>
                    <?php endif; ?>

                    <div class="single__page__x">
                        <div class="single__page__social__wrap">
                            <div class="single__page__social__title"><?= __('Поделиться');?>:</div>
                            <div class="single__page__social">
                                <a href="">
                                    <img src="<?= get_template_directory_uri(); ?>/images/icons/social/news/1.svg">
                                </a>
                                <a href="">
                                    <img src="<?= get_template_directory_uri(); ?>/images/icons/social/news/2.svg">
                                </a>
                                <a href="">
                                    <img src="<?= get_template_directory_uri(); ?>/images/icons/social/news/3.svg">
                                </a>
                            </div>
                        </div>
                        <div class="single__page__content">
                            <div class="default__text">
                                <?php the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('parts/components/back_link'); ?>
</main>





<div class="another__news js__scroll_wrap">

    <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 10,
            'orderby' => 'rand',
        );
        $the_query = new WP_Query( $args );
    ?>


    <?php if ( $the_query>have_posts() ):?>
    <div class="top_news__line top_news__line--another_news">
        <div class="container">
            <div class="top_news__title__"><?= __('Новости Cleverbots');?></div>
            <div class="top_news__line__info">
                <p class='section_x__news__date__uppercase'>
                    <b><?= __('Сегодня');?>:</b> <?= getDateWeekName(0,date('w'));?>
                </p>
                <p><?= date('d');?>, <?= getNameMonth(0,intval(date('m'))-1);?>, <?= date('Y г, H:i:s');?></p>
            </div>
        </div>
    </div>

    <div class="another__news__list">
        <div class="another__news__list__inner decore__scroll__wrap">
            <div class="new__item_y_wrap js__scrollbar js__scrollbar_padding">

                <?php while ( $the_query->have_posts() ): ?>
                <?php $the_query->the_post(); ?>
                <div class="new__item_y">
                    <?php get_template_part('parts/news/slider'); ?>
                </div>
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>

            </div>
        </div>
    </div>
    <?php endif;?>


    <div class="archive_link__block">
        <div class="container">
            <div class="archive_link__inner">
                <a href="<?= get_term_link(1, 'category');?>" class="archive_link__x">
                    <span><?= __('Архив новостей');?></span>
                    <img src="<?= get_template_directory_uri(); ?>/images/icons/arrow_pagination/next.svg">
                </a>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>