<?php
/*
Template Name: Контакты
 */
?>

<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>
<?php get_breadcrumb(); ?>
<?php the_post();?>

<main class="main_x">
	<div class="main_x__inner main_x--decore">
		<?php get_template_part('parts/components/side_line'); ?>
        <div class="top_page">
			<div class="container">
				<div class="top_page__line">
					<h1 class="title__"><?php the_title();?></h1>
				</div>
			</div>
		</div>
		<div class="contact_page__content">
			<div class="container">
				<div class="section_x__map__inner">
					<div class="section_x__map__inner_x"></div>
					<div class="section_x__map__inner_info">
						<div class="section_x__map__inner_info__item section_x__map__inner_info__item--full">
							<div class="section_x__map__info_t"><?= __('Наш адрес');?></div>
							<div class="section_x__map__info_adress"><?= get_field('adress','option');?></div>
						</div>
						<div class="section_x__map__inner_info__item">
							<div class="section_x__map__info_t"><?= __('Контактный телефон');?></div>
							<a href="tel:<?= get_field('phone','option');?>" class="section_x__map__info_phone"><?= get_field('phone','option');?></a>
						</div>
						<div class="section_x__map__inner_info__item">
							<div class="section_x__map__info_t"><?= __('Электронная почта');?></div>
							<a href="mailto:<?= get_field('email','option');?>" class="section_x__map__info_mail"><?= get_field('email','option');?></a>
						</div>
						<div class="section_x__map__inner_info__item section_x__map__inner_info__item--social">
							<div class="section_x__map__info_t"><?= __('Мы в соц.сетях');?>:</div>
							<div class="section_x__map__info_social">
								<?php $social = get_field('socai_list','option');?>
								<a href="<?= $social['tg'];?>" target='_blank'>
									<img src="<?= get_template_directory_uri(); ?>/images/icons/social/color/tg.svg">
									<span>TELEGRAM</span>
								</a>
								<a href="<?= $social['lin'];?>" target='_blank'>
									<img src="<?= get_template_directory_uri(); ?>/images/icons/social/color/id.svg">
									<span>linkedin</span>
								</a>
								<a href="<?= $social['fb'];?>" target='_blank'>
									<img src="<?= get_template_directory_uri(); ?>/images/icons/social/color/fb.svg">
									<span>FACEBOOK</span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer();?>