<?php
/*
Template Name: О нас
 */
?>

<?php get_header();?>
<?php get_template_part('parts/header__block');?>
<?php get_breadcrumb();?>
<?php the_post();?>


<main class="main_x">
	<div class="main_x__inner main_x--decore">
    <?php get_template_part('parts/components/side_line'); ?>
        <div class="team__another__man">
            <div class="container">
                <?php
                    $args = array(
                        'post_type' => 'personal',
                        'posts_per_page' => -1
                    );
                    $the_query = new WP_Query( $args );
                ?>

                <?php while ( $the_query->have_posts() ): ?>
                    <?php $the_query->the_post(); ?>
                    <?php get_template_part('parts/components/card_team'); ?>
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <?php get_template_part('parts/callback'); ?>
	</div>
</main>
<?php get_footer();?>