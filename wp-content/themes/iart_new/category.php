<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>

<main class="main_x">
    <div class="main_x__inner main_x--decore main_x__inner--mbottom">

        <?php get_template_part('parts/components/side_line'); ?>

        <div class="top_page">
            <div class="container">
                <div class="top_page__line top_page__line--news">
                    <h1 class="title__"><?= __('Новости Cleverbots');?></h1>
                    <div class="top_page__line__info">
                        <p class='section_x__news__date__uppercase'>
                            <b><?= __('Сегодня');?>:</b> <?= getDateWeekName(0,date('w'));?>
                        </p>
                        <p><?= date('d');?>, <?= getNameMonth(0,intval(date('m'))-1);?>, <?= date('Y г, H:i:s');?></p>
                    </div>

                </div>
                <div class="top_page__filter__line">
                    <div class="select__news select__filter">
                        <select class="js_select js_select__link">
                            <?php $currentID = $wp_query->get_queried_object()->term_id;?>

                            <?php
                                $terms = get_terms([
                                        'taxonomy' =>'category',
                                        'hide_empty' => false,
                                        'orderby' => 'id',
                                        'order' => 'ASC'
                                ])
                            ?>
                            <?php foreach( $terms as $term ):?>
                            <option <?php if ($currentID == $term->term_id):?> selected <?php endif;?>value="<?= get_term_link($term);?>"><?= $term->name;?></option>
                            <?php endforeach;?>
                        </select>
                    </div>

                    <div class="top_page__filter__news">
                        <?php foreach( $terms as $term ):?>
                        <a <?php if ($currentID == $term->term_id):?>class="active" <?php endif;?>href="<?= get_term_link($term);?>"><?= $term->name;?></a>
                        <?php endforeach;?>
                    </div>
                    <a href="#" class="top_page__filter__subscribe">
                        <img src="<?= get_template_directory_uri(); ?>/images/icons/mail_subscribe.svg">
                        <span><?= __('Подписка на новости');?></span>
                    </a>
                </div>
            </div>
        </div>


        <?php if ($posts) : ?>
        <div class="news__wrap">
            <div class="container">
                <div class="news__list">
                    <?php foreach ($posts as $post) : setup_postdata ($post); ?>
                    <?php get_template_part('parts/news/item'); ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <?php else:?>
        <div class="page_y">
            <div class="container">
                <div class="page_u_inner">
                    <div class="default__text">
                        <h2 class="title_y title_y--center"><?= __('В данном разделе пока нет новостей');?></h2>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php get_template_part('parts/components/pagination'); ?>
    <?php get_template_part('parts/callback'); ?>
</main>
<?php get_footer();?>