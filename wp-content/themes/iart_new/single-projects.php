<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>
<?php the_post();?>

<main class="main_x">
    <div class="main_x__inner main_x--decore">
        <?php get_template_part('parts/components/side_line'); ?>

        <div class="top_page">
            <div class="container">
                <h1 class="title__"><?php the_title();?></h1>
                <div class="project__hashtags">
                    <?php $term_list = wp_get_post_terms( $post->ID, 'projects__type', array('fields' => 'all') );?>
                    <?php foreach ($term_list as $term):?>
                    <a href="<?= get_term_link( $term );?>">#<?= $term->name;?></a>
                    <?php endforeach;?>
                </div>
            </div>
        </div>

        <div class="project__x">
            <div class="container">
                <div class="project__x__inner">
                    <div class="project__x__images__wrap">
                        <div class="project__x__arrows js_project__x__arrows">
                            <button class="project__x__arrow project__x__arrow--prev">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/arrows_project/prev.svg">
                            </button>
                            <button class="project__x__arrow project__x__arrow--next">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/arrows_project/next.svg">
                            </button>
                        </div>
                        <div class="project__x__slider__wrap">
                            <div class="project__x__slider js_project__x__slider">
                                <?php $images = get_field('slider'); ?>
                                <?php foreach( $images as $image ): ?>
                                <div class="project__x__image__item">
                                    <a href="<?= $image['url']?>" title="<?php the_title();?>">
                                        <img src="<?= $image['url']?>">
                                    </a>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                    <?php $info = get_field('information');?>
                    <div class="project__x__info">

                        <?php if ($info['task']):?>
                        <div class="project__x__info__item">
                            <div class="project__x__info__image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/project/1.svg">
                            </div>
                            <div class="project__x__info__data">
                                <div class="project__x__info__title"><?= __('Задача');?></div>
                                <div class="project__x__info__text">
                                    <?= $info['task'];?>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>

                        <?php if ($info['solution']):?>
                        <div class="project__x__info__item">
                            <div class="project__x__info__image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/project/2.svg">
                            </div>
                            <div class="project__x__info__data">
                                <div class="project__x__info__title"><?= __('Решение');?></div>
                                <div class="project__x__info__text">
                                    <?= $info['solution'];?>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>

                        <?php if ($info['result']):?>
                        <div class="project__x__info__item">
                            <div class="project__x__info__image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/project/3.svg">
                            </div>
                            <div class="project__x__info__data">
                                <div class="project__x__info__title"><?= __('Результат');?></div>
                                <div class="project__x__info__text">
                                    <?= $info['result'];?>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>

                    </div>

                    <a href="" class="btn_default btn_style_1 btn_discuss"><?= __('Обсудить проект');?></a>
                </div>
            </div>
        </div>




        <?php get_template_part('parts/components/back_link'); ?>
    </div>
    <?php get_template_part('parts/callback'); ?>
</main>


<?php get_footer();?>