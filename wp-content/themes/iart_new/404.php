<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>
<div class="not__found__page">
    <?php get_template_part('parts/components/side_line'); ?>
    <div class="not__found__page__wrap">
        <div class="container">
            <div class="not__found__page__info">
                <h1 class="not__found__page__title"><?= __('Страница не найдена');?></h1>
                <div class="not__found__page__text"><?= __('К сожалению данная страница не найдена или была удалена. Перейдите на главную страницу для поиска необходимой информации.');?></div>
                <div class="not__found__page__add">
                    <div class="not__found__page__add__text"><?= __('Возможно, вы ищете это');?>:</div>
                    <div class="not__found__page__add__links">
                        <a href="<?= get_page_link(6);?>">— <?= __('О компании');?></a>
                        <a href="<?= get_post_type_archive_link( 'projects' );?>">— <?= __('Реализованные проекты');?></a>
                        <a href="">— <?= __('Связаться');?></a>
                    </div>
                </div>
                <a href="<?= get_site_url();?>" class="btn_default btn_style_1 btn_not__found"><?= __('Вернуться на главную');?></a>
            </div>
            <div class="not__found__page__image">
                <img src="<?= get_template_directory_uri(); ?>/images/content/404.gif">
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>