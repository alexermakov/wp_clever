<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>
<main class="main_x">
    <div class="main_x__inner main_x--decore">

        <?php get_template_part('parts/components/side_line'); ?>

        <div class="top_page">
            <div class="container">
                <h1 class="title__"><?php single_term_title();?></h1>
            </div>
        </div>

        <div class="page_y">
            <div class="container">
                <div class="page_u_inner">
                    <div class="default__text">
                        <?php the_content();?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>

<?php get_footer();?>