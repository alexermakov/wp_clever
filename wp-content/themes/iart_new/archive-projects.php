<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>
<?php get_breadcrumb(); ?>

<main class="main_x">
    <div class="main_x__inner main_x--decore">
        <?php get_template_part('parts/components/side_line'); ?>

        <div class="top_page">
            <div class="container">
                <h1 class="title__"><?= post_type_archive_title();?></h1>
            </div>
        </div>

        <div class="page_y page_y--projects">

            <?php get_template_part('parts/projects/feautured_block'); ?>

            <div class="page_projects__main">
                <div class="container">

                    <?php if($hasFeautured):?>
                    <div class="page_projects__list__title">
                        <?= __('Другие проекты');?>
                    </div>
                    <?php endif;?>

                    <div class="page_projects__filter">
                        <?php get_template_part('parts/components/filter'); ?>
                        <?php get_template_part('parts/components/filter__medium'); ?>
                    </div>
                    <div class="page_projects__list__wrap">
                        <?php if ($posts) : ?>

                        <div class="page_projects__list">
                            <?php foreach ($posts as $post) : setup_postdata ($post); ?>
                            <?php get_template_part('parts/projects/item'); ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="btn_load_more_projects__wrap">
                            <a href="javascript:void(0);" class="btn_load_more_projects">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/load.svg">
                                <span><?= __('ЗАГРУЗИТЬ ЕЩЕ');?></span>
                            </a>
                        </div>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php get_template_part('parts/callback'); ?>
</main>

<?php get_footer();?>