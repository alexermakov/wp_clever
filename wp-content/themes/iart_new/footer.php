<!-- start footer -->
<div class="footer__block">
    <?php if(get_page_template_slug( get_the_ID())!='page-templates/page_contact.php'):?>
    <section class="section_x section_x__map">
        <div class="container">
            <div class="section_x__map__title"><?= __('Люди созданы для творчества');?></div>
            <div class="section_x__map__subtitle"><?= __('предоставьте остальное искусственному интеллекту');?></div>
            <div class="section_x__map__inner">
                <div class="section_x__map__inner_x"></div>
                <div class="section_x__map__inner_info">
                    <div class="section_x__map__inner_info__item section_x__map__inner_info__item--full">
                        <div class="section_x__map__info_t"><?= __('Наш адрес');?></div>
                        <div class="section_x__map__info_adress"><?= get_field('adress','option');?></div>
                    </div>
                    <div class="section_x__map__inner_info__item">
                        <div class="section_x__map__info_t"><?= __('Контактный телефон');?></div>
                        <a href="tel:<?= get_field('phone','option');?>" class="section_x__map__info_phone"><?= get_field('phone','option');?></a>
                    </div>
                    <div class="section_x__map__inner_info__item">
                        <div class="section_x__map__info_t"><?= __('Электронная почта');?></div>
                        <a href="mailto:<?= get_field('email','option');?>" class="section_x__map__info_mail"><?= get_field('email','option');?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif;?>

    <a href="javascript:void(0)" class="js_bot__launch bot__launch hide_launch_bot">
        <img src="<?= get_template_directory_uri(); ?>/images/icons/bot/launch.svg">
    </a>

    <footer class="main_footer">
        <div class="container">
            <div class="main_footer__copyright">© <?= date("Y");?> <?= __('Cleverbots. Все права защищены.');?></div>
            <div class="main_footer__social">
                <?php get_template_part('parts/components/social'); ?>
            </div>
            <button class="btn__up js_btn__up">
                <div class="btn__up__text"><?= __('Наверх');?></div>
                <div class="btn__up__view"></div>
            </button>
        </div>
    </footer>
</div>


<?php wp_footer(); ?>
<script>
    const themePath = "<?php echo get_template_directory_uri();?>";
</script>
<?php get_template_part('parts/modal'); ?>
</body>

</html>