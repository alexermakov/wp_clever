<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>

<main class="main_x">
    <div class="main_x__inner main_x__inner--vacancy main_x--decore">
        <?php get_template_part('parts/components/side_line'); ?>

        <div class="top_page">
            <div class="container">
                <h1 class="title__"><?= post_type_archive_title();?></h1>
            </div>
        </div>

        <div class="vacancy__wrap">
            <div class="container">
                <div class="vacancy__list">
                    <?php if ($posts) : ?>
                    <?php foreach ($posts as $post) : setup_postdata ($post); ?>
                    <?php get_template_part('parts/vacancy/item'); ?>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>

    <?php get_template_part('parts/components/pagination'); ?>
    <?php get_template_part('parts/callback'); ?>

</main>
<?php get_footer();?>