<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>
<?php the_post();?>


<main class="main_x">
    <div class="main_x__inner main_x--decore">


        <?php get_template_part('parts/components/side_line'); ?>


        <div class="top_page">
            <div class="container">
                <h1 class="title__"><?php the_title();?></h1>
                <div class="team__add__ref"><?= get_field('position');?></div>
            </div>
        </div>


        <div class="team__main__info">
            <div class="container">
                <div class="team__main__info__inner">
                    <div class="team__main__info__inner__image">
                        <?php the_post_thumbnail('full');?>
                    </div>

                    <div class="team__main__info__inner__top">
                        <div class="team__main__title"><?= __('Резюме и Достижения');?></div>
                        <div class="team__main__text">
                            <?= get_field('text_0');?>
                        </div>
                    </div>

                    <div class="team__main__add__info">

                        <?php if(get_field('education')):?>
                        <div class="team__main__add__info__item">
                            <div class="team__main__add__info__item__image">
                                <img src="<?= get_template_directory_uri(); ?>/images/icons/personal/1.svg">
                            </div>
                            <div class="team__main__add__info__item__wrap">
                                <div class="team__main__add__info__title">
                                    <?= __('Образование');?>
                                </div>
                                <div class="team__main__add__info__text"><?= get_field('education');?></div>
                            </div>
                        </div>
                        <?php endif;?>

                    </div>
                </div>
            </div>
        </div>


        <?php get_template_part('parts/personal/posts_personal'); ?>
        <?php get_template_part('parts/personal/speech'); ?>
        <?php get_template_part('parts/personal/another_personal'); ?>


        <?php get_template_part('parts/components/back_link'); ?>
        <?php get_template_part('parts/callback'); ?>
    </div>
</main>
<?php get_footer();?>