<!DOCTYPE html>
<?php $lang = 0;?>
<html>

<head>

    <meta charset="UTF-8">
    <meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />

    <meta name="facebook-domain-verification" content="ktoqpxo4c7rc94y6bk2umnedz72gb0" />

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KSLVQG2');
    </script>
    <!-- End Google Tag Manager -->


    <link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-iOS-180.png">
    <link rel="apple-touch-icon" sizes="167x167" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-iOS-167.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-iOS-152.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-iOS-120.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-Web-192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-Web-32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-Web-96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri(); ?>/images/favicons/favicon-Web-16.png">

    <meta name="msapplication-TileColor" content="#a673ff">
    <meta name="theme-color" content="#a673ff">



    <title><?php wp_title( '-', true, 'right' );bloginfo('name');?></title>
    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>