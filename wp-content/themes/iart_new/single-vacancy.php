<?php get_header();?>
<?php get_template_part('parts/header__block'); ?>

<?php get_breadcrumb(); ?>


<main class="main_x">
    <div class="main_x__inner main_x--decore">
        <?php get_template_part('parts/components/side_line'); ?>
        <div class="top_page">
            <div class="container">
                <h1 class="title__"><?php the_title();?></h1>
                <div class="vacancy__page__info__add">

                    <?php if (get_field('type')):?>
                    <div class="vacancy__page__info__item">
                        <img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/1.svg">
                        <span><?= get_field('type');?></span>
                    </div>
                    <?php endif;?>

                    <?php if (get_field('experience')):?>
                    <div class="vacancy__page__info__item">
                        <img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/2.svg">
                        <span><?= get_field('experience');?></span>
                    </div>
                    <?php endif;?>

                    <?php if (get_field('city')):?>
                    <div class="vacancy__page__info__item">
                        <img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/3.svg">
                        <span><?= get_field('city');?></span>
                    </div>
                    <?php endif;?>

                    <?php if (get_field('type_2')):?>
                    <div class="vacancy__page__info__item">
                        <img src="<?= get_template_directory_uri(); ?>/images/icons/vacancy/4.svg">
                        <span><?= get_field('type_2');?></span>
                    </div>
                    <?php endif;?>

                </div>
            </div>
        </div>

        <div class="vacancy__page__x">
            <div class="container">
                <div class="vacancy__page__x_inner">
                    <?php if (get_field('text')):?>
                    <div class="vacancy__page__block">
                        <div class="vacancy__page__title"><?= __('Описание вакансии');?></div>
                        <div class="vacancy__page__info">
                            <?= get_field('text');?>
                        </div>
                    </div>
                    <?php endif;?>

                    <?php if (get_field('task')):?>
                    <div class="vacancy__page__block">
                        <div class="vacancy__page__title"><?= __('Ваши задачи');?></div>
                        <div class="vacancy__page__info">
                            <?= get_field('task');?>
                        </div>
                    </div>
                    <?php endif;?>

                    <?php if (get_field('expectations')):?>
                    <div class="vacancy__page__block">
                        <div class="vacancy__page__title"><?= __('Наши ожидания');?></div>
                        <div class="vacancy__page__info">
                            <?= get_field('expectations');?>
                        </div>
                    </div>
                    <?php endif;?>
                </div>

                <?php get_template_part('parts/vacancy/form'); ?>

            </div>
        </div>
    </div>
    <?php get_template_part('parts/components/back_link'); ?>
</main>

<?php get_footer();?>